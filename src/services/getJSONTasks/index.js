export default function getJSONTasks() {
    const calendarData = require('../../data/input.json')

    return calendarData.map(task => {
        const [hours, mins] = task.start.split(':');
        return {
            ...task,
            startTime: parseInt(hours) * 60 + parseInt(mins)
        };
    }).sort((a, b) => a.startTime - b.startTime);
}
