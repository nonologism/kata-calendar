/**
 * behold, the algorithm
 * @param tasks: {*[
 *     {
 *         startTime: number,
 *         duration: number,
 *         id: number
 *     }
 * ]}
 *
 * @returns {*[
 *     {
 *         columns: number,
 *         tasks: [{
 *              displayColumn: number,
 *              startTime: number,
 *              duration: number,
 *              id: number
 *         }]
 *     }
 * ]}
 */
export default function getDisplayableTasks(tasks) {
    const taskGroups = [];

    let columnCounter = [0], // counts the latest time of stacked tasks. Length is the current number of columns in the agenda
        // groups tasks by column number
        taskGroup = {
            columns: 1,
            tasks: []
        };

    tasks.forEach((task, i) => {
        const minIndex = columnCounter.reduce((imin, value, index, array) => value >= array[imin] ? imin : index, -1),
            { startTime, duration } = task,
            gapped = Math.max(...columnCounter) <= startTime; // is the current start time higher than any of the columns end time

        // if true, then next task will take full width. Next tasks will be in another group, as they may be wider
        if (gapped && i > 0) {
            taskGroups.push(taskGroup);
            taskGroup = {
                columns: 1,
                tasks: [{
                    ...task,
                    displayColumn: 0
                }]
            };
            columnCounter = [startTime + duration]
        } else {
            // No room for current task. We need to add a column to the task group
            if(columnCounter[minIndex] > startTime) {
                columnCounter.push(startTime + duration);
                taskGroup.columns ++;
                taskGroup.tasks.push({
                    ...task,
                    displayColumn: columnCounter.length - 1
                });
            // There's room for current task. We place it in the column with lowest date value
            } else {
                columnCounter[minIndex] = startTime + duration;
                taskGroup.tasks.push({
                    ...task,
                    displayColumn: minIndex
                });
            }
        }
    });
    taskGroups.push(taskGroup);

    return taskGroups;
}
