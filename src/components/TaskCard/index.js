import './Card.css'
import useWindowDimensions from "../../services/useWindowDimensions";

export default function TaskCard({ displayableTask, calendarLength, columns }) {
    const { startTime, duration, id, displayColumn } = displayableTask,
        { height } = useWindowDimensions(),
        timescale = height / calendarLength;

    return (
        <div className={'card'}
             style={{
                 height: `${duration * timescale}px`,
                 width: `${100 / columns}%`,
                 top: `${startTime * timescale - 540 * timescale}px`,
                 left: `${displayColumn * 100 / columns}%`
             }}
        >
            {id}
        </div>
    );
}
