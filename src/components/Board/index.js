import getDisplayableTasks from "../../services/getDisplayableTasks";
import getJSONTasks from "../../services/getJSONTasks";
import TaskCard from "../TaskCard";

export default function Board() {
    const tasks = getJSONTasks(),
        latestTask = tasks.reduce((latest, task) => task.startTime + task.duration > latest ? task.startTime + task.duration : latest, 0),
        calendarLength = latestTask - tasks.at(0).startTime,
        calendarData = getDisplayableTasks(tasks);

    return (
        <>
            {calendarData.flatMap(taskGroup =>
                taskGroup.tasks.map((task) => (
                    <TaskCard displayableTask={task} columns={taskGroup.columns} calendarLength={calendarLength} key={task.id} />
                ))
            )}
        </>
    );
}
